extends KinematicBody2D


enum s {FOLLOWING_PATH,SEARCHING,PURSUING,PREPARING_ATTACK,ATTACKING}
var state = s.FOLLOWING_PATH

var sight_range = 160
var view_field = 0.4
var speed = 100
var lunge_speed = 300

onready var navigation : Navigation2D = find_parent("World").get_navigation()
onready var player : KinematicBody2D = find_parent("World").get_player()

var path_id = 0
var path : Curve2D
var path_index = 0

var lunge_dir : Vector2

# Called when the node enters the scene tree for the first time.
func _ready():
	set_path(0)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	$Vision.cast_to = to_local(player.global_position).clamped(sight_range)
	match state:
		s.FOLLOWING_PATH:
			following_path(delta)
		s.SEARCHING:
			searching(delta)
		s.PURSUING:
			pursuing(delta)
		s.PREPARING_ATTACK:
			waiting()
		s.ATTACKING:
			attacking(delta)

func following_path(delta):
	if path.get_point_count() != 0:
		if (position-path.get_point_position(path_index)).length() <2:
			path_index +=1
			if path_index == path.get_point_count():
				path_index = 0
		var patrol_path := navigation.get_simple_path(position,path.get_point_position(path_index))
		var movement :Vector2 = (patrol_path[1]-position).normalized()*speed
		move_and_slide(movement)
		if movement.length() >0.1:
			$Direction.cast_to=movement
	if abs($Vision.cast_to.angle_to($Direction.cast_to)) < view_field:
		if $Vision.get_collider() == player:
			set_state(s.PURSUING)

func searching(delta):
	if abs($Vision.cast_to.angle()) < view_field:
		if $Vision.get_collider() == player:
			print("i_see_you")
	

func pursuing(delta):
	var player_path := navigation.get_simple_path(position,player.position)
	if player_path.size() != 0:
		var movement = (player_path[1]-position).normalized()*speed
		move_and_slide(movement)
		if movement.length() >0.1:
			$Direction.cast_to=movement
	
	if $AttackCooldown.is_stopped() && $Vision.get_collider() == player:
		$AttackDelay.start()
		set_state(s.PREPARING_ATTACK)

func waiting():
	pass

func attacking(delta):
	move_and_slide(lunge_dir)

func set_state(new_state : int):
	state = new_state
	match new_state:
		s.PURSUING:
			$AttackCooldown.start()
		s.PREPARING_ATTACK:
			lunge_dir = (player.position-position).normalized()*lunge_speed
		s.ATTACKING:
			$AttackDuration.start()

func set_path(id):
	path_id = id
	path = find_parent("World").get_monster_path(id)
	var a = path.get_baked_points()
	var aaa


func _on_AttackCooldown_timeout():
	pass # Replace with function body.


func _on_AttackDelay_timeout():
	if state == s.PREPARING_ATTACK:
		set_state(s.ATTACKING)


func _on_AttackDuration_timeout():
	set_state(s.PURSUING)
