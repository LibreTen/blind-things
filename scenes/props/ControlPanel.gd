extends AnimatedSprite


func _input(event):
	if event.is_action_pressed("ui_accept"):
		play("default")


func _on_AnimatedSprite_frame_changed():
	if frame == 1:
		$Light2D.energy = 1.5
		$Light2D.color = Color(1.5,1,1)
	elif frame == 7:
		$Light2D.color = Color(1,1.5,1)
	else:
		$Light2D.color += Color(0,0,0.2)
