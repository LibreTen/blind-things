extends Node2D

var beast_visible = false

onready var minimap_pointer = $CanvasLayer/ViewportContainer/Viewport/Pointer
onready var player = $Player

export var ship_size = 100

# Called when the node enters the scene tree for the first time.
func _ready():
	if !OS.is_debug_build():
		$Navigation2D/MonsterPath.hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	update_map()

func update_map():
	minimap_pointer.player_rotation = player.position.x/ship_size
#	while minimap_pointer.player_rotation < 0:
#		minimap_pointer.player_rotation += PI
#	while minimap_pointer.player_distance > PI:
#		minimap_pointer.player_rotation -= PI

func get_player(): return $Player
func get_navigation(): return $Navigation2D
func get_monster_path(id): return get_node("TileMap/"+str(id)).curve
