extends Sprite3D

onready var origin := translation
const max_radius = 2.5
var player_distance = 2.5
var player_rotation = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	translation = origin + Vector3(player_distance,0,0).rotated(Vector3(0,0,1),player_rotation)
