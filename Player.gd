extends KinematicBody2D

export var speed = 10000

var has_flashlight = true

var flashlight_active = false

func _input(event):
	if event.is_action_pressed("light"):
		if has_flashlight:
			flashlight_toggle()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var movement := Vector2(0,0)
	movement.x = Input.get_action_strength("right") - Input.get_action_strength("left")
	movement.y = Input.get_action_strength("down") - Input.get_action_strength("up")
	
	movement = movement.normalized()
	
	move_and_slide(movement*speed*delta)

func flashlight_toggle():
	flashlight_active = !flashlight_active
	if flashlight_active:
		$PassiveLight.hide()
		$Flashlight.show()
	else:
		$PassiveLight.show()
		$Flashlight.hide()
